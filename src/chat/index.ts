import express from 'express';
import { EventEmitter } from 'events';
import bodyParser from 'body-parser';

class Chat extends EventEmitter {}

export const createChat = (app: express.Application): void => {
  const chat = new Chat();

  app.post('/chat', bodyParser.json(), (req, res) => {
    // Emit event to the chat
    chat.emit('message', req.body);
    res.sendStatus(200);
  });

  app.get('/chat', (req, res) => {
    // Keep request opened until `message` event from the chat
    chat.once('message', (data) => {
      res.send({ ...data, timestamp: Number(new Date()) });
    });
  });
};
