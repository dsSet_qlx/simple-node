/**
 * File contains utils to work with a static content, which placing in `assets` directory
 */
import { ReadStream } from 'fs';
import { last, split } from 'lodash';
import { ASSET_CHUNK_SIZE, getAssetPath, getImagePath } from './config';
import { getStaticContent } from './static';

/**
 * Retrieve file from the assets dir
 * @param path
 * @param encoding
 */
export const getAsset = async (path: string, encoding?: BufferEncoding): Promise<ReadStream> => {
  return getStaticContent(path, ASSET_CHUNK_SIZE, encoding);
};

/**
 * Converts url string to the asset file path and return file content if exists
 * @param url
 * @param encoding
 */
export const getAssetByUrl = async (
  url: string,
  encoding?: BufferEncoding,
): Promise<ReadStream> => {
  const path = getAssetPath(`.${url}`);
  return getStaticContent(path, ASSET_CHUNK_SIZE, encoding);
};

/**
 * Get image by name
 * @param name
 * @param encoding
 */
export const getImage = async (name: string, encoding?: BufferEncoding): Promise<ReadStream> => {
  return getAsset(getImagePath(name), encoding);
};

/**
 * Split image name to retrieve extension
 * @param name
 */
export const getImageExt = (name: string): string => {
  return last(split(name, '.')) || 'png';
};
