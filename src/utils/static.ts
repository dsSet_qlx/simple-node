import { access, lstat } from 'fs/promises';
import { constants, createReadStream, ReadStream } from 'fs';

/**
 * Retrieve file from the file system.
 * @param path
 * @param highWaterMark
 * @param encoding
 */
export const getStaticContent = async (
  path: string,
  highWaterMark?: number,
  encoding?: BufferEncoding,
): Promise<ReadStream> => {
  const [stats] = await Promise.all([lstat(path), access(path, constants.R_OK)]);

  if (!stats.isFile()) {
    throw new Error(`"${path}" Not a file`);
  }

  return createReadStream(path, { highWaterMark, encoding });
};
