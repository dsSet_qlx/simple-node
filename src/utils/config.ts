import { resolve } from 'path';
import dotenv from 'dotenv';

dotenv.config();

/**
 * List of application directories
 */
export const ROOT_DIR = resolve(__dirname, '../../');
export const ASSETS_DIR = resolve(ROOT_DIR, 'assets');
export const LOG_DIR = resolve(ROOT_DIR, 'logs');

/**
 * Functions to retrieve absolute file path
 * @param name
 */
export const getImagePath = (name: string): string => resolve(ASSETS_DIR, 'images', name);
export const getAssetPath = (name: string): string => resolve(ASSETS_DIR, name);

/**
 * Environment variables
 */
export const APP_PORT = Number.parseInt(process.env.APP_PORT, 10);
export const ASSET_CHUNK_SIZE = Number.parseInt(process.env.ASSET_CHUNK_SIZE, 10);
export const LOG_LEVEL = process.env.LOG_LEVEL || 'error';
