import winston, { Logger } from 'winston';
import { LOG_DIR } from './config';

/**
 * Function for custom log format
 */
const errorFormatter = winston.format.printf(({ level, message, metadata }) => {
  const { timestamp, stack, id } = metadata;

  const formatted = `${timestamp} [${level}]\t${id || ''}: ${message}`;

  return stack
    ? `${formatted}\n==========STACKTRACE==========\n${stack}\n==========STACKTRACE [end]==========`
    : formatted;
});

/**
 * Create logger instance
 * @param level
 */
export const createLogger = (level: string): Logger => {
  return winston.createLogger({
    level,
    transports: [
      // Stream all logs to console output
      new winston.transports.Console({
        format: winston.format.combine(
          winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSS' }),
          winston.format.errors({ stack: true }),
          winston.format.metadata(),
          winston.format.colorize(),
          errorFormatter,
        ),
      }),
      // Stream all logs to file output
      new winston.transports.File({
        filename: 'combined.log',
        dirname: LOG_DIR,
        format: winston.format.combine(
          winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSS' }),
          winston.format.errors({ stack: true }),
          winston.format.metadata(),
          errorFormatter,
        ),
      }),
      // Stream error log to the separated file
      new winston.transports.File({
        filename: 'error.log',
        dirname: LOG_DIR,
        level: 'error',
        format: winston.format.combine(
          winston.format.timestamp({ format: 'YYYY-MM-DD HH:mm:ss.SSS' }),
          winston.format.errors({ stack: true }),
          winston.format.metadata(),
          errorFormatter,
        ),
      }),
      // Stream logs to the separated file in logstash format
      new winston.transports.File({
        filename: 'json.log',
        dirname: LOG_DIR,
        level: 'info',
        format: winston.format.combine(
          winston.format.errors({ stack: true }),
          winston.format.metadata(),
          winston.format.logstash(),
        ),
      }),
    ],
  });
};

type LogFn = (message: string, ...meta: Array<unknown>) => void;

/**
 * Custom logger to use in application context
 * This logger will be extended with `request id`, so we will able to track log messages for every request separately
 */
export interface RequestLogger {
  info: LogFn;
  warn: LogFn;
  error: LogFn;
  debug: LogFn;
}

/**
 * Wrap log function to have request `id` at the log metadata. See `errorFormatter` for this variable displaying.
 */
const idDecorator =
  (id: string, fn: LogFn): LogFn =>
  (message, ...meta) =>
    fn(message, { id }, ...meta);

/**
 * Produce request logger
 * @param id
 * @param logger
 */
export const createRequestLogger = (id: string, logger: Logger): RequestLogger => ({
  debug: idDecorator(id, logger.debug),
  info: idDecorator(id, logger.info),
  warn: idDecorator(id, logger.warn),
  error: idDecorator(id, logger.error),
});
