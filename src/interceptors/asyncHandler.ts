import { RequestHandler } from 'express';

/**
 * Function to wrap async middleware. Current express version does not supports async middlewares. So express does not handle errors from such functions.
 * Every async middleware should be wrapped with that function to process errors with `next` callback.
 * @param middleware
 */
export const asyncHandler =
  (middleware: RequestHandler): RequestHandler =>
  (request, response, next) => {
    return Promise.resolve(middleware(request, response, next)).catch((error) => next(error));
  };
