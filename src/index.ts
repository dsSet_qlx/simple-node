import express, { Application } from 'express';
import { APP_PORT, LOG_LEVEL } from './utils/config';
import { createLogger, RequestLogger } from './utils/logger';
import { createRequestLogMiddleware, errorErrorMiddleware } from './middleware';
import { router } from './routes';
import { createChat } from './chat';

// See `sample.http` for the list of samples.

/**
 * Extend express Request object typings with a custom fields
 */
declare global {
  // eslint-disable-next-line @typescript-eslint/no-namespace
  namespace Express {
    interface Request {
      id: string;
      logger: RequestLogger;
    }
  }
}

// Create logger instance
const logger = createLogger(LOG_LEVEL);

// Create and run main process
(async () => {
  const app: Application = express();

  createChat(app);

  // Apply middlewares
  app.use(createRequestLogMiddleware(logger));

  // Apply router
  app.use('/', router);

  // Add error handler. Should be applied at the end.
  app.use(errorErrorMiddleware);

  // Start application
  app.listen(APP_PORT, () => {
    logger.info(`Listening ${APP_PORT} port, http://localhost:${APP_PORT}`);
  });
})().catch((error) => {
  logger.error(error);
});
