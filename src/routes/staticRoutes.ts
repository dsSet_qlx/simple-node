import express from 'express';
import { getImage, getImageExt } from '../utils/assets';
import { asyncHandler } from '../interceptors';

export const router = express.Router();

router.get(
  '/:name',
  asyncHandler(async (request, response) => {
    // TODO: get image by name
    const { name } = request.params;
    const file = await getImage(name);
    file.pipe(response);
  }),
);

router.get(
  '/base64/:name',
  asyncHandler(async (request, response) => {
    const { name } = request.params;
    const file = await getImage(name, 'base64');

    // TODO: sample string
    // `<h1>${name}</h1><img src="data:image/${getImageExt(name)};base64," /><p>Rendered By Express Response!</p>`

    response.write(`<h1>${name}</h1><img src="data:image/${getImageExt(name)};base64,`);

    file.on('end', () => {
      response.write(`" /><p>Rendered By Express Response!</p>`);
      response.end();
    });

    file.pipe(response, { end: false });
  }),
);
