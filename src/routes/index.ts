import express from 'express';
import { router as simpleRoutes } from './simpleRoutes';
import { router as staticRoutes } from './staticRoutes';
import { getAssetByUrl } from '../utils/assets';
import { asyncHandler } from '../interceptors';
import { notFoundErrorHandler, notFoundMiddleware } from '../middleware';

export const router = express.Router();

router.use('/simple', simpleRoutes);

router.use('/images', staticRoutes, notFoundErrorHandler);

router.get(
  '/*',
  asyncHandler(async (request, response, next) => {
    // await new Promise((resolve) => {
    //   setTimeout(resolve, 400);
    // });
    const file = await getAssetByUrl(request.url);
    file.pipe(response, { end: true });
  }),
  notFoundErrorHandler,
);

router.all('/*', notFoundMiddleware);
