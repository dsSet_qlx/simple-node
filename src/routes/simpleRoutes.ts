import express from 'express';

export const router = express.Router();

router.get('/hello', (request, response) => {
  // TODO: Sends simple json response
  response.json({ data: 'Hello world' });
});

router.get('/error-code', (request, response) => {
  // TODO: Sends bad request
  response.sendStatus(400);
});

router.get('/throw-error', () => {
  // TODO: Sends bad request
  throw new Error('Custom error message');
});

router.post('/echo', (request, response) => {
  // TODO: echo endpoint. Sends request body back.
  response.set('Content-Type', request.get('Content-Type'));
  request.pipe(response);
});
