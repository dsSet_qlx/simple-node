import { RequestHandler } from 'express';
import { v1 as uuid } from 'uuid';
import { Logger } from 'winston';
import { createRequestLogger } from '../utils/logger';

/**
 * Function to create logger middleware.
 * The new middleware extends default request with custom context fields.
 * Middleware responsibilities:
 * 1. Log every request and response details
 * 2. Set up custom context fields
 * 3. Check request duration
 */
export const createRequestLogMiddleware =
  (logger: Logger): RequestHandler =>
  async (request, response, next) => {
    const startTime = Number(new Date());

    request.id = uuid();
    request.logger = createRequestLogger(request.id, logger);

    request.logger.info(`Request started, ${request.method} ${request.url}`);

    response.on('close', () => {
      const endTime = Number(new Date());
      const requestDuration = endTime - startTime;
      if (requestDuration > 300) {
        request.logger.warn(`Request time ${requestDuration}ms`);
      } else {
        request.logger.info(`Request time ${requestDuration}ms`);
      }
      request.logger.info(`Request finished with status code ${response.statusCode}`);
    });

    next();
  };
