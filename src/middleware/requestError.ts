import { ErrorRequestHandler } from 'express';

/**
 * Global error handler. Responsibilities:
 * 1. Write all app errors to the log
 * 2. Set correct response status code
 * 3. Close response if needed
 */
export const errorErrorMiddleware: ErrorRequestHandler = (error, request, response, next) => {
  // Log error
  request.logger.error(error);

  // Check is status code correct
  if (!response.statusCode || response.statusCode < 400) {
    response.status(500);
  }

  // Check is response ended
  if (!response.writableEnded) {
    response.json('Something went wrong');
  }
};
