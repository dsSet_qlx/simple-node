import { ErrorRequestHandler, RequestHandler } from 'express';

// Simple middleware to return 404 response.
export const notFoundMiddleware: RequestHandler = (request, response) => {
  response.status(404).json('Resource not found');
};

// Handle express error and respond with 404 status code instead of 500.
export const notFoundErrorHandler: ErrorRequestHandler = (error, request, response, next) => {
  response.status(404).json('Resource not found');
  next(error);
};
