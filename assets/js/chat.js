/* eslint-disable max-classes-per-file */
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    function adopt(value) { return value instanceof P ? value : new P(function (resolve) { resolve(value); }); }
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : adopt(result.value).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (this && this.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = op[0] & 2 ? y["return"] : op[0] ? y["throw"] || ((t = y["return"]) && t.call(y), 0) : y.next) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [op[0] & 2, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};
var _this = this;
var Connection = /** @class */ (function () {
    function Connection(url) {
        var _this = this;
        this.url = url;
        this.retriesCount = 0;
        this.handleDataReceive = function () {
            if (_this.inputRequest.status === 502) {
                _this.reconnect();
            }
            else if (_this.inputRequest.status >= 400) {
                _this.handleError();
            }
            else {
                if (_this.messageCallback) {
                    try {
                        _this.messageCallback(JSON.parse(_this.inputRequest.responseText));
                    }
                    catch (error) {
                        console.error(error);
                    }
                }
                _this.reconnect();
            }
        };
        this.reconnect = function () {
            _this.inputRequest.open('GET', _this.url);
            _this.inputRequest.send();
        };
        this.handleError = function () { return __awaiter(_this, void 0, void 0, function () {
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (this.retriesCount > 2) {
                            return [2 /*return*/];
                        }
                        return [4 /*yield*/, this.pause()];
                    case 1:
                        _a.sent();
                        this.retriesCount += 1;
                        this.reconnect();
                        return [2 /*return*/];
                }
            });
        }); };
        this.pause = function (delay) {
            if (delay === void 0) { delay = 1000; }
            return new Promise(function (resolve) { return setTimeout(resolve, delay); });
        };
        this.inputRequest = new XMLHttpRequest();
        this.inputRequest.addEventListener('load', this.handleDataReceive);
        this.inputRequest.addEventListener('timeout', this.reconnect);
        this.inputRequest.addEventListener('error', this.handleError);
        this.outputRequest = new XMLHttpRequest();
    }
    Object.defineProperty(Connection.prototype, "isConnected", {
        get: function () {
            return (this.inputRequest.readyState !== XMLHttpRequest.UNSENT ||
                this.inputRequest.readyState !== XMLHttpRequest.DONE);
        },
        enumerable: false,
        configurable: true
    });
    Connection.prototype.connect = function () {
        this.retriesCount = 0;
        this.reconnect();
    };
    Connection.prototype.disconnect = function () {
        this.inputRequest.abort();
    };
    Connection.prototype.sendMessage = function (message) {
        this.outputRequest.open('POST', this.url);
        this.outputRequest.setRequestHeader('Content-Type', 'application/json');
        this.outputRequest.send(JSON.stringify(message));
    };
    Connection.prototype.onReceiveMessage = function (callback) {
        this.messageCallback = callback;
    };
    return Connection;
}());
var Chat = /** @class */ (function () {
    function Chat(document, name) {
        var _this = this;
        this.document = document;
        this.name = name;
        this.connection = new Connection('http://localhost:8080/chat');
        this.onMessage = function (message) {
            var messageContainer = _this.document.createElement('div');
            messageContainer.classList.add('message');
            var nameContainer = _this.document.createElement('div');
            nameContainer.classList.add('name');
            if (message.name === _this.name) {
                messageContainer.classList.add('self');
                nameContainer.append('Me:');
            }
            else {
                nameContainer.append("".concat(message.name, ":"));
            }
            messageContainer.append(nameContainer, message.message);
            _this.content.append(messageContainer);
            messageContainer.scrollIntoView({ behavior: 'smooth' });
        };
        this.handleSendButtonClick = function () {
            if (!_this.input.value) {
                return;
            }
            _this.connection.sendMessage({ name: _this.name, message: _this.input.value });
            _this.input.value = null;
        };
        this.container = document.getElementById('chat');
        this.content = document.getElementById('content');
        this.input = document.getElementById('input');
        this.sendButton = document.getElementById('sendButton');
        this.connection.onReceiveMessage(this.onMessage);
        this.sendButton.addEventListener('click', this.handleSendButtonClick);
    }
    Chat.prototype.connect = function () {
        this.connection.connect();
        this.connection.sendMessage({ name: this.name, message: 'Hello!' });
    };
    return Chat;
}());
(function () { return __awaiter(_this, void 0, void 0, function () {
    var name, chat;
    return __generator(this, function (_a) {
        switch (_a.label) {
            case 0:
                name = "MoonWalker-".concat(Math.round(Math.random() * 100));
                chat = new Chat(document, name);
                return [4 /*yield*/, chat.connect()];
            case 1:
                _a.sent();
                return [2 /*return*/];
        }
    });
}); })();
