/* eslint-disable max-classes-per-file */

interface Message {
  message: string;
  name: string;
}

class Connection {
  private readonly inputRequest: XMLHttpRequest;

  private readonly outputRequest: XMLHttpRequest;

  private retriesCount = 0;

  private messageCallback?: (message: Message) => void;

  get isConnected(): boolean {
    return (
      this.inputRequest.readyState !== XMLHttpRequest.UNSENT ||
      this.inputRequest.readyState !== XMLHttpRequest.DONE
    );
  }

  constructor(public readonly url: string) {
    this.inputRequest = new XMLHttpRequest();
    this.inputRequest.addEventListener('load', this.handleDataReceive);
    this.inputRequest.addEventListener('timeout', this.reconnect);
    this.inputRequest.addEventListener('error', this.handleError);

    this.outputRequest = new XMLHttpRequest();
  }

  connect() {
    this.retriesCount = 0;
    this.reconnect();
  }

  disconnect() {
    this.inputRequest.abort();
  }

  sendMessage(message: Message) {
    this.outputRequest.open('POST', this.url);
    this.outputRequest.setRequestHeader('Content-Type', 'application/json');
    this.outputRequest.send(JSON.stringify(message));
  }

  onReceiveMessage(callback: (message: Message) => void) {
    this.messageCallback = callback;
  }

  private handleDataReceive = () => {
    if (this.inputRequest.status === 502) {
      this.reconnect();
    } else if (this.inputRequest.status >= 400) {
      this.handleError();
    } else {
      if (this.messageCallback) {
        try {
          this.messageCallback(JSON.parse(this.inputRequest.responseText));
        } catch (error) {
          console.error(error);
        }
      }
      this.reconnect();
    }
  };

  private reconnect = () => {
    this.inputRequest.open('GET', this.url);
    this.inputRequest.send();
  };

  private handleError = async () => {
    if (this.retriesCount > 2) {
      return;
    }
    await this.pause();
    this.retriesCount += 1;
    this.reconnect();
  };

  private pause = (delay = 1000) => {
    return new Promise((resolve) => setTimeout(resolve, delay));
  };
}

class Chat {
  private container: HTMLDivElement;

  private content: HTMLDivElement;

  private input: HTMLInputElement;

  private sendButton: HTMLButtonElement;

  private connection = new Connection('http://localhost:8080/chat');

  constructor(private document: Document, private readonly name: string) {
    this.container = document.getElementById('chat') as HTMLDivElement;
    this.content = document.getElementById('content') as HTMLDivElement;
    this.input = document.getElementById('input') as HTMLInputElement;
    this.sendButton = document.getElementById('sendButton') as HTMLButtonElement;
    this.connection.onReceiveMessage(this.onMessage);

    this.sendButton.addEventListener('click', this.handleSendButtonClick);
  }

  private onMessage = (message: Message) => {
    const messageContainer = this.document.createElement('div');
    messageContainer.classList.add('message');

    const nameContainer = this.document.createElement('div');
    nameContainer.classList.add('name');

    if (message.name === this.name) {
      messageContainer.classList.add('self');
      nameContainer.append('Me:');
    } else {
      nameContainer.append(`${message.name}:`);
    }

    messageContainer.append(nameContainer, message.message);

    this.content.append(messageContainer);

    messageContainer.scrollIntoView({ behavior: 'smooth' });
  };

  private handleSendButtonClick = () => {
    if (!this.input.value) {
      return;
    }

    this.connection.sendMessage({ name: this.name, message: this.input.value });
    this.input.value = null;
  };

  public connect() {
    this.connection.connect();
    this.connection.sendMessage({ name: this.name, message: 'Hello!' });
  }
}

(async () => {
  const name = `MoonWalker-${Math.round(Math.random() * 100)}`;

  const chat = new Chat(document, name);
  await chat.connect();
})();
